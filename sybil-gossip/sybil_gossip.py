import web3
import json

gasPrice = 100000000000


def dotted2Hex(part):
    hexxed = "".join(map(web3.Web3.toHex, map(int, part.split("."))))
    hexxed = hexxed.replace("0x", "")
    return f"0x{hexxed}"


def new_aliases():
    ip = "TEST IP"
    port = 30312
    nodeids = [
        "TEST NODE ID"
    ]

    enodes = []
    # for i in range(100):
    for nodeid in nodeids:
        enodes.append(web3.Web3.toBytes(text=f"{nodeid}@{ip}:{port}"))

    return enodes


if __name__ == "__main__":
    w3 = web3.Web3(web3.Web3.HTTPProvider("http://127.0.0.1:7545"))
    print(f"Is connected : {w3.isConnected()}")
    print(f"Block number: {w3.eth.blockNumber}")

    account = "0xA7130Fa32D14E79c04EBd9De6880a4b36ed7fD9F"

    balance = w3.eth.getBalance(account)
    print(f"Ether Balance: {w3.fromWei(balance, 'ether')}")

    abi = json.loads(
        '[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"enodes","type":"bytes[]"}],"name":"Aliases","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"enodes","type":"bytes[]"}],"name":"Groups","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"subnet","type":"bytes[]"},{"indexed":false,"internalType":"bytes[][]","name":"last_bytes_port_nodeid","type":"bytes[][]"}],"name":"Subnet","type":"event"},{"inputs":[{"internalType":"address","name":"_addressToAllow","type":"address"}],"name":"addUser","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_subnet","type":"bytes[]"},{"internalType":"bytes[][]","name":"_last_bytes_port_nodeid","type":"bytes[][]"}],"name":"SubnetPublish","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_enodes","type":"bytes[]"}],"name":"GroupsPublish","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_enodes","type":"bytes[]"}],"name":"AliasesPublish","outputs":[],"stateMutability":"nonpayable","type":"function"}]'
        # '[{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"enodes","type":"bytes[]"}],"name":"Aliases","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"enodes","type":"bytes[]"}],"name":"Groups","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"subnet","type":"bytes[]"},{"indexed":false,"internalType":"bytes[][]","name":"last_bytes_port_nodeid","type":"bytes[][]"}],"name":"Subnet","type":"event"},{"inputs":[{"internalType":"bytes[]","name":"_subnet","type":"bytes[]"},{"internalType":"bytes[][]","name":"_last_bytes_port_nodeid","type":"bytes[][]"}],"name":"SubnetPublish","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_enodes","type":"bytes[]"}],"name":"GroupsPublish","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_enodes","type":"bytes[]"}],"name":"AliasesPublish","outputs":[],"stateMutability":"nonpayable","type":"function"}]'
    )
    address = w3.toChecksumAddress('0xd850dEa786cdF0fB3a73Ca0cD6b884Dc72aD151f')#"0x0982a6e54C0eD9646D79Ca1ce7B4332cB3e878D3")

    contract = w3.eth.contract(address=address, abi=abi)

    enodes = new_aliases()

    tx_hash = contract.functions.AliasesPublish(enodes).transact(
        {"from": account}
    )
    # Wait for transaction to be mined
    tx_receipt = w3.eth.wait_for_transaction_receipt(tx_hash)

    print(f"{w3.fromWei(tx_receipt.gasUsed * gasPrice, 'ether')} ETH used")
