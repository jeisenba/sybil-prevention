// SPDX-License-Identifier: MIT
pragma solidity 0.8.11;

contract SybilGossip {
  address owner; // Address of the owner of the smart contract
  // List of addresses allowed to publish
  mapping(address => bool) allowedAddr;

  event Subnet(bytes[] subnet, bytes[][] last_bytes_port_nodeid);
  event Groups(bytes[] enodes);
  event Aliases(bytes[] enodes);

  constructor() {
    owner = msg.sender;
    allowedAddr[owner] = true;
  }

  modifier onlyOwner() {
    require(msg.sender == owner, "Caller is not the owner");
    _;
  }

  /*Add the given address to the list of allowed addresses
  */
  function addUser(address _addressToAllow) public onlyOwner {
    allowedAddr[_addressToAllow] = true;
  }

  modifier isAllowed(address _address) {
    require(allowedAddr[_address], "You need to be allowed");
    _;
  }

  /*Publish the information of suspicious nodes in the same subnet
    _subnet: [subnet1, subnet2, subnet3, ...]
    _last_bytes_port_nodeid: [
      [byte_port_nodeid1_1, byte_port_nodeid1_2, ...],
      [byte_port_nodeid2_1, ...], 
      [byte_port_nodeid3_1, byte_port_nodeid3_2, ...], ...
    ]
  */
  function SubnetPublish(
    bytes[] memory _subnet, bytes[][] memory _last_bytes_port_nodeid
    ) public isAllowed(msg.sender) {
      emit Subnet(_subnet, _last_bytes_port_nodeid);
  }
  /*Publish the information of nodes that are too close in the DHT
    _enodes: [enodes1, enodes2, enodes3, ...]
  */
  function GroupsPublish(bytes[] memory _enodes)
    public isAllowed(msg.sender) {
      emit Groups(_enodes);
  }
  /*Publish the information of nodes that have too many ID (aliases)
    _enodes: [enodes1, enodes2, enodes3, ...]
  */
  function AliasesPublish(bytes[] memory _enodes)
    public isAllowed(msg.sender) {
      emit Aliases(_enodes);
  }
}