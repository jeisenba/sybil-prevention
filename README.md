# Sybil Prevention

This repositery contains 2 programs and a smart-contract.

Sybil-Gossip is a Python program that retrieved information of suspicious nodes
from [Crawleth](https://gitlab.inria.fr/jeisenba/crawleth) and trigger the corresponding Event of the smart-contract

Sybil-Revocation is a Python program that parses the events' logs and remove the connections of an Ethereum node
to any suspicious nodes
