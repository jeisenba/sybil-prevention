# Max size of discovery packets.
DISCOVERY_MAX_PACKET_SIZE = 1280

# Buffer size used for incoming discovery UDP datagrams (must be larger than
# DISCOVERY_MAX_PACKET_SIZE)
DISCOVERY_DATAGRAM_BUFFER_SIZE = DISCOVERY_MAX_PACKET_SIZE * 2

KADEMLIA_PUBLIC_KEY_SIZE = 512
# UDP packet constants.
MAC_SIZE = 256 // 8  # 32
SIG_SIZE = 520 // 8  # 65
HEAD_SIZE = MAC_SIZE + SIG_SIZE  # 97
EXPIRATION = 60  # let messages expire after N secondes
PROTOCOL_VERSION = 4

CMD_PING = 1
CMD_PONG = 2
CMD_FIND_NODE = 3
CMD_NEIGHBOURS = 4