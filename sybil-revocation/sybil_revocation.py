from typing import Dict, List
from itertools import chain
from collections import defaultdict
import pprint
from web3 import Web3
import time
import json
import redis
import requests

from multiprocessing import Pool
from functools import partial
from collections import defaultdict
from protocol.protocol import Connection
from protocol_utils import int_to_big_endian, KADEMLIA_PUBLIC_KEY_SIZE

redis_db = redis.StrictRedis(host='127.0.0.1', port=6379, db=0, charset="utf-8", decode_responses=True)

conn = Connection(redis_db)
conn.open()

def log_loop(event_filter, poll_interval):
    while True:
        for event in event_filter.get_new_entries():
            handle_event(event)
        time.sleep(poll_interval)


def handle_event(event):
    print(event)


def verify_enode_existence(enode):
    redis_db.flushdb()
    enode = Web3.toText(enode)
    print(f"Verifying existence of {enode}")
    nodeid = enode.split("@")[0]
    target_ip, target_port = enode.split("@")[1].split(":")
    target = f"{target_ip}-{target_port}-{nodeid}"
    # print(target)
    remote, port = YOUR_GETH_NODE_IP, YOUR_GETH_NODE_PORT
    conn.send_find_node_v4((remote, int(port), int(port)), nodeid)
    found = False
    while(not found):
        while(redis_db.scard('neighbours') == 0):
            time.sleep(1)
        neighbours = redis_db.smembers('neighbours')
        for neighbour in neighbours:
            if neighbour.split("-")[-1] == nodeid:
                print("FOUND !")
                target_found = f"{nodeid}@{neighbour.split('-')[0]}:{neighbour.split('-')[1]}"
                found = True
        else:
            print(f"NOT FOUND (added size is {redis_db.scard('added')}, neighbours size is {redis_db.scard('neighbours')})!")
            for neighbour in neighbours:
                redis_db.srem('neighbours', neighbour)
                remote, port, _ = neighbour.split("-")
                conn.ping((remote, int(port), int(port)))
            time.sleep(1)
            for neighbour in neighbours:
                remote, port, _ = neighbour.split("-")
                conn.send_find_node_v4((remote, int(port), int(port)), nodeid)
            time.sleep(1)
    return target_found


if __name__ == "__main__":
    threshold = 2
    abi = json.loads(
        '[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"enodes","type":"bytes[]"}],"name":"Aliases","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"enodes","type":"bytes[]"}],"name":"Groups","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"subnet","type":"bytes[]"},{"indexed":false,"internalType":"bytes[][]","name":"last_bytes_port_nodeid","type":"bytes[][]"}],"name":"Subnet","type":"event"},{"inputs":[{"internalType":"address","name":"_addressToAllow","type":"address"}],"name":"addUser","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_subnet","type":"bytes[]"},{"internalType":"bytes[][]","name":"_last_bytes_port_nodeid","type":"bytes[][]"}],"name":"SubnetPublish","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_enodes","type":"bytes[]"}],"name":"GroupsPublish","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_enodes","type":"bytes[]"}],"name":"AliasesPublish","outputs":[],"stateMutability":"nonpayable","type":"function"}]'
        # '[{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"enodes","type":"bytes[]"}],"name":"Aliases","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"enodes","type":"bytes[]"}],"name":"Groups","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes[]","name":"subnet","type":"bytes[]"},{"indexed":false,"internalType":"bytes[][]","name":"last_bytes_port_nodeid","type":"bytes[][]"}],"name":"Subnet","type":"event"},{"inputs":[{"internalType":"bytes[]","name":"_subnet","type":"bytes[]"},{"internalType":"bytes[][]","name":"_last_bytes_port_nodeid","type":"bytes[][]"}],"name":"SubnetPublish","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_enodes","type":"bytes[]"}],"name":"GroupsPublish","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes[]","name":"_enodes","type":"bytes[]"}],"name":"AliasesPublish","outputs":[],"stateMutability":"nonpayable","type":"function"}]'
    )
    contractAddress = '0xd850dEa786cdF0fB3a73Ca0cD6b884Dc72aD151f' #'0x0982a6e54C0eD9646D79Ca1ce7B4332cB3e878D3'
    w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:7545'))
    contract = w3.eth.contract(address=contractAddress, abi=abi)
    accounts = w3.eth.accounts
    # print(contract, "contract")

    # block_filter = w3.eth.filter('latest')
    # log_loop(block_filter, 2)
    event = contract.events.Aliases.createFilter(fromBlock=0, toBlock='latest')
    print(event)

    eventlist = event.get_all_entries()
    print(len(eventlist))

    remote, port = YOUR_GETH_NODE_IP, YOUR_GETH_NODE_PORT
    conn.ping((remote, int(port), int(port)))
    time.sleep(1)

    with Pool() as p:
        targets_found = p.map(verify_enode_existence, eventlist[-1]['args']['enodes'])

    conn.close()
    # print(eventlist[-1]['args']['enodes'])
    print(f"Targets found !")
    print(f"Revoking targets from my Geth node...")

    targets_counter = defaultdict(int)
    for target_found in targets_found:
        ip = target_found.split('@')[1].split(':')[0]
        targets_counter[ip] += 1

    for i, target_found in enumerate(targets_found):
        ip = target_found.split('@')[1].split(':')[0]
        nodeid_ip = target_found.split(':')[0]
        if targets_counter[ip] >= threshold:
            req_json = {"jsonrpc":"2.0","method":"admin_peers","params":[],"id":i}
            r = requests.post(YOUR_GETH_NODE_RPC_CONNECTION_INFO, json=req_json)
            print("Removing target if currently connected...")
            for peerinfo in json.loads(r.content)['result']:
                if peerinfo['enode'].startswith(f"enode://{nodeid_ip}"):
                    req_json = {"jsonrpc":"2.0","method":"admin_removePeer","params":[f"{peerinfo['enode']}"],"id":i}
                    r = requests.post(YOUR_GETH_NODE_RPC_CONNECTION_INFO, json=req_json)
                    print(f"RPC answer is: {json.loads(r.content)['result']}")
            print("Prevent future connections...")
            req_json = {"jsonrpc":"2.0","method":"admin_removePeer","params":[f"enode://{target_found}"],"id":i}
            r = requests.post(YOUR_GETH_NODE_RPC_CONNECTION_INFO, json=req_json)
            print(f"RPC answer is: {json.loads(r.content)['result']}")
        else:
            print(f"Not superior to threshold {threshold}")
    print("Targets successfully revoked !")
