import traceback
import socket
import threading
import sys
import time
import ipaddress
from typing import Tuple, Sequence, Any

import rlp
from eth_keys import datatypes
from eth_typing import Hash32
from eth_keys.datatypes import PublicKey
from eth_utils import encode_hex
from p2p import ecies

from protocol_utils import pack_v4, CMD_PING, int_to_big_endian, KADEMLIA_PUBLIC_KEY_SIZE, \
    CMD_PONG, CMD_FIND_NODE, DISCOVERY_DATAGRAM_BUFFER_SIZE, unpack_v4, \
    CMD_NEIGHBOURS, extract_nodes_from_payload, PROTOCOL_VERSION, enc_port, \
    MAC_SIZE, big_endian_to_int


class Connection(object):
    def __init__(self, redis_db):
        self.from_addr = ("127.0.0.1", 50001, 50001)
        self.expiration: int = 60
        self.private_key: datatypes.PrivateKey = ecies.generate_privkey()
        self.socket: socket = None
        self.serve_thread = None
        self.redis_db = redis_db
        """print("public")
        print(self.private_key.public_key)
        print(self.private_key.public_key.to_hex())
        print(self.private_key.public_key.to_bytes())
        print(self.private_key.public_key.to_address())
        print(self.private_key.public_key.to_canonical_address())
        print(self.private_key.public_key.to_checksum_address())
        print(self.private_key.public_key.to_compressed_bytes())
        print("private")
        print(self.private_key)"""
        # sys.exit(0)

    def open(self):
        self.socket = socket.socket(socket.AF_INET,
                                    socket.SOCK_DGRAM)
        self.socket.settimeout(self.expiration)
        self.socket.bind(('', 50001))
        print(f"Socket opened and binded on port {50001}")
        self.serve_thread = threading.Thread(target=self.serve_forever)
        # self.serve_thread.daemon = True
        self.serve_thread.start()

    def close(self):
        if self.socket:
            sock = self.socket
            self.socket = None
            try:
                sock.shutdown(socket.SHUT_RDWR)
                print("Socket closed")
            except socket.error:
                pass
            finally:
                sock.close()

    def send(self, remote: Tuple[str, int, int], cmd_id: int, payload: Sequence[Any]):
        """
            Pack the given payload using the given msg type and send it over our socket.

            If we get an OSError from our socket when attempting to send it, that will be logged
            and the message will be lost.
            """
        message = pack_v4(cmd_id, payload, self.private_key)
        try:
            self.socket.sendto(message, (remote[0], remote[1]))
        except Exception:
            if self.socket:
                print(f"Unexpected error when sending msg to {remote}:")
                traceback.print_exc(file=sys.stderr)
        return message

    def _get_msg_expiration(self):
        return rlp.sedes.big_endian_int.serialize(int(time.time() + self.expiration))

    def _is_msg_expired(self, rlp_expiration: bytes) -> bool:
        expiration = rlp.sedes.big_endian_int.deserialize(rlp_expiration)
        if time.time() > expiration:
            print('Received message already expired')
            return True
        return False

    def ping(self, remote: Tuple[str, int, int]):
        version = rlp.sedes.big_endian_int.serialize(PROTOCOL_VERSION)
        expiration = self._get_msg_expiration()
        this_address = [ipaddress.ip_address(self.from_addr[0]).packed, enc_port(self.from_addr[1]),
                        enc_port(self.from_addr[2])]
        node_address = [ipaddress.ip_address(remote[0]).packed, enc_port(remote[1]), enc_port(remote[2])]
        payload = (version, this_address, node_address, expiration)
        message = self.send(remote, CMD_PING, payload)
        # Return the msg hash, which is used as a token to identify pongs.
        token = Hash32(message[:MAC_SIZE])

        # print(f'>>> ping (v4) {remote} (token == {encode_hex(token)})')

    def send_pong_v4(self, remote: Tuple[str, int, int], token: Hash32) -> None:
        expiration = self._get_msg_expiration()
        # print(f'>>> pong {remote}')
        node_address = [ipaddress.ip_address(remote[0]).packed, enc_port(remote[1]),
                        enc_port(remote[2])]
        payload = (node_address, token, expiration)
        self.send(remote, CMD_PONG, payload)

    def send_find_node_v4(self, remote: Tuple[str, int, int], nodeid: str) -> None:
        target_key = int_to_big_endian(int(nodeid, base=16)).rjust(KADEMLIA_PUBLIC_KEY_SIZE // 8, b"\x00")
        if len(target_key) != KADEMLIA_PUBLIC_KEY_SIZE // 8:
            raise ValueError(f"Invalid FIND_NODE target ({target_key!r}). Length is not 64")
        expiration = self._get_msg_expiration()
        # print(f'>>> find_node {target_key} to {remote}')
        self.send(remote, CMD_FIND_NODE, (target_key, expiration))

    def serve_forever(self) -> None:
        print("Serving...")
        while self.socket:
            try:
                datagram, (ip_address, port) = self.socket.recvfrom(DISCOVERY_DATAGRAM_BUFFER_SIZE)
                threading.Thread(target=self.consume_datagram, args=[(ip_address, port, port), datagram]).run()
            except TimeoutError:
                self.close()
                exit(0)
            except ConnectionResetError as e:
                print(e)
            except Exception as e:
                if self.socket:
                    print(e)

    def consume_datagram(self, address: Tuple[str, int, int], datagram):
        self.handle_msg(address, datagram)

    def handle_msg(self, address: Tuple[str, int, int], message: bytes) -> None:
        try:
            remote_pubkey, cmd_id, payload, message_hash = unpack_v4(message)
        except SyntaxError as e:
            print('Error unpacking message (%s) from %s: %s', message, address, e)
            return

        if cmd_id == CMD_PING:
            return self.recv_ping_v4(address, remote_pubkey, payload, message_hash)
        elif cmd_id == CMD_PONG:
            return self.recv_pong_v4(address, remote_pubkey, payload, message_hash)
        elif cmd_id == CMD_FIND_NODE:
            return
        elif cmd_id == CMD_NEIGHBOURS:
            return self.recv_neighbours_v4(address, payload, message_hash)
        else:
            raise ValueError(f"Unknown command id: {cmd_id}")

    def recv_ping_v4(
            self, remote: Tuple[str, int, int], remote_pubkey: PublicKey, payload: Sequence[Any],
            message_hash: Hash32) -> None:
        """Process a received ping packet.

        A ping packet may come any time, unrequested, or may be prompted by us bond()ing with a
        new node. In the former case we'll just reply with a pong, whereas in the latter we'll
        also send an empty msg on the appropriate channel from ping_channels, to notify any
        coroutine waiting for that ping.

        Also, if we have no valid bond with the given remote, we'll trigger one in the background.
        """
        # The ping payload should have at least 4 elements: [version, from, to, expiration], with
        # an optional 5th element for the node's ENR sequence number.
        if len(payload) < 4:
            print('Ignoring PING msg with invalid payload: %s', payload)
            return
        elif len(payload) == 4:
            _, _, _, expiration = payload[:4]
            enr_seq = None
        else:
            _, _, _, expiration, enr_seq = payload[:5]
            enr_seq = big_endian_to_int(enr_seq)
        # print('<<< ping(v4) from %s, enr_seq=%s', remote, enr_seq)
        self._is_msg_expired(expiration)

        self.send_pong_v4(remote, message_hash)

    def recv_pong_v4(self, remote: Tuple[str, int, int], remote_pubkey: PublicKey, payload: Sequence[Any],
                     _: Hash32) -> None:
        # The pong payload should have at least 3 elements: to, token, expiration
        if len(payload) < 3:
            print('Ignoring PONG msg with invalid payload: %s', payload)
            return
        elif len(payload) == 3:
            _, token, expiration = payload[:3]
            enr_seq = None
        else:
            _, token, expiration, enr_seq = payload[:4]
            enr_seq = big_endian_to_int(enr_seq)
        self._is_msg_expired(expiration)

        # print(f'<<< pong (v4) from {remote} (token == {encode_hex(token)})')

        # Send FindNode if the node haven't send a Ping back

    def recv_neighbours_v4(self, remote: Tuple[str, int, int], payload: Sequence[Any], _: Hash32) -> None:
        # The neighbours payload should have 2 elements: nodes, expiration
        if len(payload) < 2:
            print('Ignoring NEIGHBOURS msg with invalid payload: %s', payload)
            return
        nodes, expiration = payload[:2]
        self._is_msg_expired(expiration)
        try:
            neighbours = extract_nodes_from_payload(remote, nodes)
        except ValueError:
            print(f"Malformed NEIGHBOURS packet from {remote}: {nodes}")
            return

        for neighbour in neighbours:
            neighbour_info = f"{neighbour[0][0]}-{neighbour[0][1]}-{neighbour[1].to_hex()[2:]}"
            # print(f"Neighbour: {neighbour_info}")
            if not self.redis_db.sismember('added', neighbour_info):
                self.redis_db.sadd('added', neighbour_info)
                self.redis_db.sadd('neighbours', neighbour_info)
